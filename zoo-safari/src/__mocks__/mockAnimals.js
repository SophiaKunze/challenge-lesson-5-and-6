export const MOCK_ANIMALS = [
  { id: 1, name: "Carlos", type: "crocodile", emoji: "🐊" },
  { id: 2, name: "Pete", type: "penguin", emoji: "🐧" },
  { id: 3, name: "Don", type: "duck", emoji: "🦆" },
];