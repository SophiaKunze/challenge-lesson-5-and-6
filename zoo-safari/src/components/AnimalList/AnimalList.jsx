// props: come from parents (cannot be shared between siblings)
// props is a JS object -> destructured to animals
const AnimalList = ({ animals }) => {
    
    // { animals: [] } <-- the array contains current value of state
    
    return (
        <ul>
            <li className="list-header">To see our favorite stars:</li>
            { animals.map(animal => <li key={animal.id}>{ animal.type } {animal.name} that looks like { animal.emoji }</li>) }
        </ul>
    );
}

export default AnimalList