const ZooHeader = ({ zooName, zooAddress }) => {
  return (
    <>
      <h1>Welcome to {zooName}</h1>
      <div>
        Come and visit us at
        <div className="zoo-address">{zooAddress}</div>
      </div>
    </>
  );
};

export default ZooHeader;
