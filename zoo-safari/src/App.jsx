import { useEffect, useState } from "react";
import "./App.css";
import { MOCK_ANIMALS } from "./__mocks__/mockAnimals"
import AnimalList from "./components/AnimalList/AnimalList"
import ZooHeader from "./components/ZooHeader/ZooHeader";

const zooName = "Safari-Zoo";
const zooAddress = "Lion-Street 5";

//[
//  { id: 1, name: "Carlos", type: "crocodile", emoji: "🐊" },
//  { id: 2, name: "Pete", type: "penguin", emoji: "🐧" },
//  { id: 3, name: "Don", type: "duck", emoji: "🦆" },
//];

// React Hooks: expose functionality to our application. Are named use... e.g. useState()
// useState() - controls local state (updates state and forces component to rerender)
// useEffect() - controls life cycles

const App = () => {
  // ComponentDidMount
  // Deconstructing
  // Immutable State: NEVER directly change state, always use e.g. setAnimals
  // setAnimals always forces rerender, therefore NEVER use state update in root component (causes infinite loop)
  const [animals, setAnimals] = useState([]);

  console.log("Animals", animals);

  // 2 arguments - i. function runs on ComponentDidMount, ii. list of dependencies (array)
  useEffect(() => {
    // create a pending state change
    setAnimals(MOCK_ANIMALS);
  }, []); // empty dependency: only run code once

  return (
    <div className="App">

      {/**display ZooHeader component*/}
      <ZooHeader zooName={zooName} zooAddress={zooAddress}/>

      {/**display AnimalList component and pass in animals as props*/}
      <AnimalList animals={animals}/>

    </div>
  );
};

export default App;
